import numpy as np
import pandas as pd
import operator
from scipy import signal
from scipy.cluster.vq import vq, kmeans, whiten
import scipy
import matplotlib.pyplot as plt
from datetime import datetime


def detect_retrofit(current,time):

# Calculate the daily throughput of the battery

    # Get the days of the Timestamps
    datearray = np.asarray(time/60/60/24, dtype='datetime64[D]')
    dates, ib = np.unique(datearray, return_index=True)
    dates = np.array(dates)

    # Calculate the dayly Capacity-throughput
    cum_current = np.nancumsum(current)
    extrema = np.split(cum_current,ib)

    fullrange = [(max(a, default=0) - min(a, default=0))/60 for a in extrema[1:]]
    fullrange = np.array(fullrange)

    # Now get the Maxima
    topline = np.zeros(len(fullrange)).astype(bool)
    topline[scipy.signal.find_peaks(fullrange)[0]] = 1

# Find the centers of the Maxima from the daily throughput

    # Filter the Topline for the input of k-means
    k_input = max_filter(fullrange,topline,dates)

    # All possible retrofit Days are in the List of Fullcharge-Days
    split_candidate = np.where(topline)[0]

    # Prepare the Error and Centerstore
    error = [0,0]
    centers = [[0,0],[0,0]]

    for k in range(1,3):
        # Calculate the centers (accumulation points)    
        k_centers = scipy.cluster.vq.kmeans(k_input,k)[0]
        k_centers = np.sort(k_centers)
        centers[k-1] = k_centers

        # Now calculate the Error for one center to compare it with the
        # Error for two centers
        if k==1:
            error[k-1] = np.sqrt(np.mean(np.power( (k_input - k_centers),2 )))

        # Calculate the errors for two centers
        if k==2:

            # Check when one of the two capacitys occur
            occur_g1 = np.where((fullrange[topline] > k_centers[0]*0.9) & (fullrange[topline] < k_centers[0]*1.1))[0]
            occur_g2 = np.where((fullrange[topline] > k_centers[1]*0.9) & (fullrange[topline] < k_centers[1]*1.1))[0]

            # Try to find the "Split" Day on which the retrofit event occured
            # If no Split day could be found (occur_g2 start before occur_g1)
            # then set the error really high and one center wins
            try:

                # At the Split day we saw the first occurence of the second center
                # from k-means algorithm
                split_day = np.where(np.greater(occur_g2[0],occur_g1))[0]
                split_day = split_candidate[occur_g1[split_day[-1]]]

                # Calculate the error of the two centers
                area_1 = np.power( k_input[0:split_day] - k_centers[0],2 )
                area_2 = np.power( k_input[split_day+1:-1] - k_centers[1],2 )

                error[k-1] = np.sqrt(np.mean( np.append(area_1,area_2) ))
            except:
                error[k-1] = np.inf

    # Compare the errors of one and two centers
    if error[0] > error[1]:
        is_retro = 1
        mean_capacity = centers[1]
    else:
        is_retro = 0
        mean_capacity = centers[0]

# Now calculate the timestep where the retrofit event occured        

    if is_retro:
        # Take the beginning of the Retrofit day as retrofit moment
        TS_retro_idx = np.where(datearray==dates[split_day])[0][-1]
        TS_retro = time[TS_retro_idx]
    else:
        # If no retrofit occur, treat the whole array as "before retrofit"
        TS_retro = time[-1]
        
# Plot the daily throughput and the calculated maxima of the throughput

    # Plot some stuff
    plt.figure()
    plt.plot(dates,fullrange,'b',dates[topline],fullrange[topline],'k*',zorder=0)
    plt.bar(dates[split_day],np.max(fullrange),color='#FF0000', width=np.rint(len(fullrange)/200), linewidth=0,zorder=1)
    plt.title('Prescan of daily Troughput and retrofit Detection')
    plt.ylabel('Throughput / Ah')
    plt.xlabel('Time')
    
    #plt.show()

# Return the calculated values    
    return mean_capacity, TS_retro, is_retro






def max_filter(fullrange,topline,dates):

# Prepare the Timeseries

    # Save the old fullrange size, we will need it for the output
    timesteps = np.arange(0, len(fullrange), 1)

    # avoid maxima from first winter or summer at the beginning of timeline
    # Therefore check the years and detect the months in the first period
    # Winter is 12-2 and Summer is 6-8

    # So first calculate the number of Halfyears
    datelist = pd.to_datetime(dates)
    h_years = len(np.unique(pd.Series.to_numpy(datelist.year)))*2
    
    # Now check the months that occur in the beginning of the Timeline
    months_candidate = pd.Series.to_numpy(datelist.month)
    cand_idx = np.arange(0, int(np.round( len(months_candidate)/h_years )), 1)
    first_months = months_candidate[cand_idx]
    
    # If monthts in this first area are from Winter or Summer (possible low throughput)
    # dont use them cause of false retrofit detection
    invalid_period = (first_months==12) | (first_months==12) | (first_months==1) | (first_months==2) | (first_months==6) | (first_months==7) | (first_months==8)
    invalid_period = np.append(invalid_period, np.zeros((len(fullrange)-len(invalid_period))) )

    # Now get the valid topline of the throughput
    topline = np.logical_and(topline, np.logical_not(invalid_period))

# Apply first filter on Fullrange

    # For that, prepare the fullrange with the topline maxima
    new_range = fullrange[topline]
    new_topline = np.zeros(len(new_range)).astype(bool)

    # Now calculate the new maxima of that
    new_topline[scipy.signal.find_peaks(new_range)[0]] = 1

    # Find out where the points of the maximum are
    idx = np.intersect1d(fullrange,new_range[new_topline],return_indices=True)[1]
    
    # Write them back to topline
    new_topline = np.zeros(len(timesteps)).astype(bool)
    new_topline[idx] = 1

    # Now Interpolate the missing values
    fullrange = np.interp(timesteps,timesteps[new_topline],fullrange[new_topline])

# Apply second filter on Fullrange

    # For that, prepare the fullrange with the topline maxima
    new_range = fullrange[new_topline]
    new_topline = np.zeros(len(new_range)).astype(bool)

    # Now calculate the new maxima of that
    new_topline[scipy.signal.find_peaks(new_range)[0]] = 1

    # Find out where the points of the maximum are
    idx = np.intersect1d(fullrange,new_range[new_topline],return_indices=True)[1]
    
    # Write them back to topline
    new_topline = np.zeros(len(timesteps)).astype(bool)
    new_topline[idx] = 1

    # Now Interpolate the missing values
    k_input = np.interp(timesteps,timesteps[new_topline],fullrange[new_topline])


    return k_input