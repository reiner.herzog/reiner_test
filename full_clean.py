import scipy.io
import os
import numpy as np
import Retrofit as ret
import fill_equal_gaps as feg
import get_Ri as Ri

# Clear Terminal
os.system('cls')

# Get the subdirectories in the working folder
path = os.listdir('.')

Folders = []
for i in path:
    if os.path.isdir(i):
        if (i != '.vscode'):
            Folders.append(i)

Foldernumber = 1
folder = Folders[Foldernumber-1]
print(folder)

# Get the files in the choosen subfolder
new_path = os.path.join(os.getcwd(), folder)

Files = []
for file in os.listdir(new_path):
    if file.endswith(".mat"):
        Files.append(file)

Filenumber = 1
system = Files[Filenumber-1]
print(system)

# Load the file and sort the values
file_path = os.path.join(new_path, system)

mat = scipy.io.loadmat(file_path)
time = np.concatenate(mat['time'])
voltage = np.concatenate(mat['voltage'])
current = np.concatenate(mat['current'])
temperature = np.concatenate(mat['temperature'])

# Settings
do_detect_retrofit = 0
do_fill_equal_gaps = 0
do_baseline_correction = 0
do_post_correction = 0
do_get_Ri = 1
do_Capacity_per_Voltage_Bin = 0

if do_Capacity_per_Voltage_Bin:
    do_baseline_correction = 1
    do_get_Ri = 1

# Store original Data
time_old = time
voltage_old = voltage
current_old = current
temperature_old = temperature

# Retrofit Detection
if do_detect_retrofit:
    mean_capacity, TS_retro, is_retro = ret.detect_retrofit(current,time)

# Fill Gaps
if do_fill_equal_gaps:
    time, voltage, current, temperature = feg.fill_equal_gaps( time, voltage, current, temperature)

# Calculate Ri's
if do_get_Ri:
    clean_Ri = Ri.get_Ri(time, voltage, current, temperature)
