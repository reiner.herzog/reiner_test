import numpy as np
from scipy import stats
from scipy import special
from scipy import interpolate as intpl
from scipy import ndimage
import pandas as pd
import matplotlib.pyplot as plt

def get_Ri(time, voltage_avg, current_avg, temperature):
    
# document the Function    

# Calculate the Ri's

    # First calculate the Voltage and Current differences
    v_diff = np.append(0,np.diff(voltage_avg))
    i_diff = np.append(0,np.diff(current_avg))
    
    # Divide the differences to get the Ri
    Ri = v_diff / i_diff
    
    # Get only Ri's that are calculated with a significant 
    # voltage and current change
    Ri[ np.less(abs(v_diff),0.25) ] = np.nan;
    Ri[ np.less(abs(i_diff),2.5) ] = np.nan;
    
    # Remove outlier vom Data
    outlier = is_outlier(Ri,'median')
    Ri[outlier] = np.nan
    
# Build Temperature and Voltage Bin Matrix
    
    # Round the Temperature to one decimal place and get the unique Values
    # With round, bankers round is meant
    rnd_temp = np.rint(np.nextafter(temperature*10, temperature*10+1)) / 10
    temp_occur = np.unique(rnd_temp)
    
    # Put the indicees of the rounded Temperature array in the temperature array
    xsorted = np.argsort(temp_occur)
    ypos = np.searchsorted(temp_occur[xsorted], rnd_temp)
    col = xsorted[ypos]
    
    # Get the Years from Unixtime
    datearray = np.asarray(time/60/60/24, dtype='datetime64[D]')
    years = pd.DatetimeIndex(datearray).year.astype(int)
    year_occur = np.unique(years)

    # Put the indicees of the extracted Years in the Dates array
    xsorted = np.argsort(year_occur)
    ypos = np.searchsorted(year_occur[xsorted], years)
    row = xsorted[ypos]
    
    # Build the matrix of Temperature and Time Bins
    xq, yq = np.meshgrid(temp_occur, year_occur)    
    
# Sort and Clean Ri's

    # Prepare the Ri Matrix and the Occurence Matrix
    Ri_Map = np.full(xq.shape , np.nan)    
    Ri_Map_occur = np.zeros( xq.shape )
    
    # Dont use nan Ri's
    valid_idx = ~np.isnan(Ri)
    Ri_nn = Ri[valid_idx]
    col_nn = col[valid_idx].astype(int)
    row_nn = row[valid_idx].astype(int)
    
    # Build the Ri_Map
    for i in range(0, len(Ri_nn)-1):
        Ri_Map[row_nn[i],col_nn[i]] = np.nanmean( [Ri_nn[i], Ri_Map[row_nn[i],col_nn[i]]] )
        
    # And count the occurences of values in the specific fields
        if(~np.isnan(Ri_nn[i])):
            Ri_Map_occur[row_nn[i],col_nn[i]] = Ri_Map_occur[row_nn[i],col_nn[i]] + 1
    
    # Trust Ri's with minimum 3 occurences
    Ri_Map[Ri_Map_occur < 3] = np.nan
    
# Interpolate, extrapolate and smooth the Ri_Map
    
    # Prepare Data to Interpolate 
    validpoints = np.argwhere(~np.isnan(Ri_Map))
    validvalues = Ri_Map[~np.isnan(Ri_Map)]
    querypoints = np.argwhere(~np.isnan(xq))
    
    # Interpolate, Extrapolate and Smooth
    vq = intpl.griddata( validpoints, validvalues, querypoints, method='linear')
    vq = extrapolate_nans(querypoints[:,0], querypoints[:,1] , vq) 
    Ri_Map[ querypoints[:,0], querypoints[:,1] ] = vq
    Ri_Map = ndimage.median_filter(Ri_Map,size=25,mode='nearest')
    
    # Plot the Ri_Map
    fig1, ax1 = plt.subplots()    
    ax1.contourf(xq,yq,Ri_Map,cmap=plt.cm.hot)
    fig1.colorbar()
    
    
# Write Ri's back to the timeline 

    clean_Ri = Ri_Map[row,col]
    
    fig2, ax2 = plt.subplots()
    ax2.plot(datearray,clean_Ri)
    
    clean_Ri = 0
    return clean_Ri


def is_outlier(data,method):
    
# This function finds outlier in data    

    if method == 'mean':
        data_nn = data[~np.isnan(data)]
        mean = np.mean(data_nn)
        standard_deviation = np.std(data_nn)
        distance_from_mean = abs(data - mean)
        max_deviations = 3
        isoutlier = distance_from_mean > max_deviations * standard_deviation
        
    elif method == 'median':
        data_nn = data[~np.isnan(data)]
        center = np.median(data_nn)
        madfactor = -1 /(np.sqrt(2)*special.erfcinv(3/2))
        amad = madfactor*np.median(abs(data_nn - center))

        lowerbound = center - 3*amad;
        upperbound = center + 3*amad;
                
        isoutlier = np.logical_or( data > upperbound, data < lowerbound )

    elif method == 'rolling_median':
        return np.zeros(len(data))
        
    else: 
        return np.zeros(len(data))

    return isoutlier


def extrapolate_nans(x, y, v):

    if np.ma.is_masked(v):
        nans = v.mask
    else:
        nans = np.isnan(v)
    notnans = np.logical_not(nans)
    v[nans] = intpl.griddata((x[notnans], y[notnans]), v[notnans],
        (x[nans], y[nans]), method='nearest').ravel()
    return v
