import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime as datetime
import traceback


def fill_equal_gaps( time, voltage, current, temperature):

# This function should fill as good as possible missing Data.
# Using the Timestamps to check if some are missing, with that we can mark missing Data
# as nan.
# The Data is then treated seperately:
# Temperature:  1.  Find all gaps (Series of nan's) in the Temperature Data
#               2.  Divide the Gaps in Short Gaps (<=2 Hours) and Long Gaps (> 2 Hours)
#               3.1.The Short Gaps will be interpolated
#               3.2.The Treatment for the long Gaps is to:
#                   check if there is another (complete) Part in the Temperature Data with
#                   the same lenght and the same Start- and End-Temperature, this found 
#                   Part will be fitted in the Gap.
#                   In this method the nearest Gapfiller will be choosen to preserve
#                   as good as possible the caracteristics of the area in which the Temperature
#                   is reconstructed.
#               4.  If no Gapfiller could be found in 3.2, we will look for another Gapfiller with
#                   variable (but shorter) lenght than the Gap which is treated.
#                   The Start and End Temperature from the Gap have to be the same. 
#                   The nearest Gapfiller will be choosen and the Rest of the Gap will be 
#                   interpolated.
#
# Voltage and Current must be treated together because they are depend on each other
# The Data is treated as followed:
#               1.  Find all gaps (Series of nan's) in the Voltage and Current Data
#               2.  Now divide the Gaps in active and passive Gaps, where the Definition
#                   for a passive Gap is: Voltage change is below 1uV per Minute, all other
#                   Gaps are active Gaps
#               3.1.The passive Gaps are Gaps where nothing happend, so the Voltage will be
#                   linear interpolated and the Current will be filled with Zeros
#               3.2.The Voltage in the active Gaps are treated like the Temperature reconstruction.
#                   There will not be divided into short and long Gaps, for every Gap
#                   

# Detect the missing Timestamps and set marker for missing Data

    # First build a dataframe to assign timestamps to the data
    df_raw = pd.DataFrame({'time':time, 'voltage':voltage, 'current':current, 'temperature':temperature})
    df_raw.set_index('time')
    
    # Resample the data, with missing values set to nan
    df_raw['time'] = pd.to_datetime(df_raw['time'], unit='s')
    df_raw.set_index('time', inplace = True)
    df = df_raw.resample('60S').ffill()

# Do some preparations before reconstructing

    # Calculating the Resolution of the Temperature
    T_res = np.abs(np.diff(temperature))
    T_res = np.round(T_res*1000)/1000
    T_res = min( T_res[np.greater(T_res,0)] )

    # Calculating the Resolution of the Voltage
    V_res = np.abs(np.diff(voltage))
    V_res = np.round(V_res*1000)/1000
    V_res = min( V_res[np.greater(V_res,0)] )

    # Calculating the Resolution of the Current  
    I_res = np.abs(np.diff(current))
    I_res = np.round(I_res*1000)/1000
    I_res = min( I_res[np.greater(I_res,0)] )

# Reconstruct Temperature



    return time, voltage, current, temperature